﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HrvatskeZeljezniceApp
{
    public class TrainManager{
        TrainDB trains = new TrainDB();

        public void DeleteTrain(Vlak selectedTrain) {
            if (selectedTrain != null){
                trains.Delete(selectedTrain);
            }
        }

        public void AddTrain(Vlak train){   
            trains.Add(train);
 
        }


    }
}
