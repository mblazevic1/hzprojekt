﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HrvatskeZeljezniceApp {

    public class TimerSettings {
        public int Time1 { get; set; } = 60;
        public int Time2 { get; set; } = 60;
        public int Time3 { get; set; } = 60;
        public bool Timer0 { get; set; }
        public int AddTime { get; set; } = 60;
       
    }

}
