using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace HrvatskeZeljezniceApp
{
    public partial class HrvatskeZeljezniceDB : DbContext
    {
        public HrvatskeZeljezniceDB()
            : base("name=HrvatskeZeljezniceDB")
        {
        }

        public virtual DbSet<Vlak> Vlakovi { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
