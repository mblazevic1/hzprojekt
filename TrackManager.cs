﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;



namespace HrvatskeZeljezniceApp {
    public class TrackManager {
        private Boolean crash = false;
        FormTrack formTrack;
        TrainManager trainManager = new TrainManager();
        public TrackManager(FormTrack formTrack) {
            this.formTrack = formTrack;
            StationPasses = new List<string>();

        }

        private Boolean checkIfEmpty(ListBox listBox) {
            if (listBox.Items.Count > 0) {
                return false;
            } else {
                return true;
            }
        }

        public void MoveTrainToStation(ListBox source, ListBox track, ListBox station, int line) {
            if (track.Enabled && station.Enabled) {
                Task.Run(() => {
                    if (source.SelectedItem != null) {
                        var selectedItem = source.SelectedItem;
                        source.Items.Remove(selectedItem);
                        
                        Thread.Sleep(1000);
                        
                        if (checkIfEmpty(track)) {
                            track.Items.Add(selectedItem);
                        } else {
                            formTrack.CallDisableLine(line);
                            crash = true;
                        }

                        Thread.Sleep(3000);

                        if (checkIfEmpty(station)) {
                            if (crash) {
                                crash = false;
                                return;
                            } else {
                                track.Items.Clear();
                                TrainArrivedAtStation(selectedItem.ToString());
                                station.Items.Add(selectedItem);
                            }
                        } else {
                            formTrack.CallDisableLine(line);
                        }

                    } else {
                        var selectedItem = source.Items[0];
                        source.Items.Remove(selectedItem);

                        Thread.Sleep(1000);

                        if (checkIfEmpty(track)) {
                            track.Items.Add(selectedItem);
                        } else {
                            formTrack.CallDisableLine(line);
                        }
                        Thread.Sleep(1000);

                        track.Items.Remove(selectedItem);
                        if (checkIfEmpty(station)) {
                            if (crash) {
                                crash = false;
                                return;
                            } else {
                                track.Items.Clear();
                                TrainArrivedAtStation(selectedItem.ToString());
                                station.Items.Add(selectedItem);
                            }
                        } else {
                            formTrack.CallDisableLine(line);
                        }
                    }
                });
            }
        }

        public void MoveTrainToExit(ListBox station, ListBox exit, Label lblExit) {
            Task.Run(() => {
                if (checkIfEmpty(exit)) {
                    var train = station.Items[0];
                    lblExit.Text = $"{train} se sprema napustiti stanicu!";
                    station.Items.Remove(station.Items[0]);
                    exit.Items.Add(train);
                    Thread.Sleep(5000);
                    lblExit.Text = $"{train} je napustio stanicu!";
                    exit.Items.Remove(train);
                    DeleteTrain((Vlak)train);


                } else {
                    exit.Items.Clear();
                    exit.Items.Add("GAME OVER!");
                    formTrack.CallDisableLine(1);
                    formTrack.CallDisableLine(2);
                    formTrack.CallDisableLine(3);
                    lblExit.Text = "RIP Hrvatske Željeznice";
                    MessageBox.Show("Uništili ste hrvatske željeznice :(");
                }

            });
        }

        private void DeleteTrain(Vlak train) {
                trainManager.DeleteTrain(train);
        }

            private void TrainArrivedAtStation(string train) {
                StationPasses.Add(train);
                TrainAtStation?.Invoke(this, new TrainMovementArgs(train));
            } 
        




        public delegate void TrainMovementDelegate(object sender, TrainMovementArgs e);
        public event TrainMovementDelegate TrainAtStation;
        public List<string> StationPasses { get; }
        public event EventHandler CloseForm;

    }
}