﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HrvatskeZeljezniceApp
{
    public class TrainMovementArgs : EventArgs
    {
        public TrainMovementArgs(string train){
            Train = train;
        }

        public string Train { get; }
    }
}
