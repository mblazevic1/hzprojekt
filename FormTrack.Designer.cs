﻿namespace HrvatskeZeljezniceApp
{
    partial class FormTrack
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbTrains = new System.Windows.Forms.ListBox();
            this.lbTrack2 = new System.Windows.Forms.ListBox();
            this.lbTrack3 = new System.Windows.Forms.ListBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.lblTimer1 = new System.Windows.Forms.Label();
            this.lblTimer2 = new System.Windows.Forms.Label();
            this.lblTimer3 = new System.Windows.Forms.Label();
            this.btnTrack1 = new System.Windows.Forms.Button();
            this.btnTrack2 = new System.Windows.Forms.Button();
            this.btnTrack3 = new System.Windows.Forms.Button();
            this.tmrTrainLoad = new System.Windows.Forms.Timer(this.components);
            this.lbTrack1 = new System.Windows.Forms.ListBox();
            this.lbExit = new System.Windows.Forms.ListBox();
            this.lbStation1 = new System.Windows.Forms.ListBox();
            this.lbStation2 = new System.Windows.Forms.ListBox();
            this.lbStation3 = new System.Windows.Forms.ListBox();
            this.btnStation1 = new System.Windows.Forms.Button();
            this.btnStation2 = new System.Windows.Forms.Button();
            this.btnStation3 = new System.Windows.Forms.Button();
            this.lblExit = new System.Windows.Forms.Label();
            this.lblCounter = new System.Windows.Forms.Label();
            this.btnSaveToFile = new System.Windows.Forms.Button();
            this.btnExitFromApp = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbTrains
            // 
            this.lbTrains.FormattingEnabled = true;
            this.lbTrains.Location = new System.Drawing.Point(25, 8);
            this.lbTrains.Name = "lbTrains";
            this.lbTrains.Size = new System.Drawing.Size(209, 498);
            this.lbTrains.TabIndex = 0;
            // 
            // lbTrack2
            // 
            this.lbTrack2.FormattingEnabled = true;
            this.lbTrack2.Location = new System.Drawing.Point(585, 46);
            this.lbTrack2.Name = "lbTrack2";
            this.lbTrack2.Size = new System.Drawing.Size(209, 56);
            this.lbTrack2.TabIndex = 1;
            // 
            // lbTrack3
            // 
            this.lbTrack3.FormattingEnabled = true;
            this.lbTrack3.Location = new System.Drawing.Point(842, 46);
            this.lbTrack3.Name = "lbTrack3";
            this.lbTrack3.Size = new System.Drawing.Size(209, 56);
            this.lbTrack3.TabIndex = 2;
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick_1);
            // 
            // timer2
            // 
            this.timer2.Interval = 1000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick_1);
            // 
            // timer3
            // 
            this.timer3.Interval = 1000;
            this.timer3.Tick += new System.EventHandler(this.timer3_Tick_1);
            // 
            // lblTimer1
            // 
            this.lblTimer1.AutoSize = true;
            this.lblTimer1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTimer1.Location = new System.Drawing.Point(420, 8);
            this.lblTimer1.Name = "lblTimer1";
            this.lblTimer1.Size = new System.Drawing.Size(46, 19);
            this.lblTimer1.TabIndex = 4;
            this.lblTimer1.Text = "01:00";
            // 
            // lblTimer2
            // 
            this.lblTimer2.AutoSize = true;
            this.lblTimer2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lblTimer2.Location = new System.Drawing.Point(669, 8);
            this.lblTimer2.Name = "lblTimer2";
            this.lblTimer2.Size = new System.Drawing.Size(46, 19);
            this.lblTimer2.TabIndex = 5;
            this.lblTimer2.Text = "01:00";
            // 
            // lblTimer3
            // 
            this.lblTimer3.AutoSize = true;
            this.lblTimer3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lblTimer3.Location = new System.Drawing.Point(923, 8);
            this.lblTimer3.Name = "lblTimer3";
            this.lblTimer3.Size = new System.Drawing.Size(46, 19);
            this.lblTimer3.TabIndex = 6;
            this.lblTimer3.Text = "01:00";
            // 
            // btnTrack1
            // 
            this.btnTrack1.Location = new System.Drawing.Point(400, 108);
            this.btnTrack1.Name = "btnTrack1";
            this.btnTrack1.Size = new System.Drawing.Size(75, 23);
            this.btnTrack1.TabIndex = 7;
            this.btnTrack1.Text = "Peron1";
            this.btnTrack1.UseVisualStyleBackColor = true;
            this.btnTrack1.Click += new System.EventHandler(this.btnTrack1_Click);
            // 
            // btnTrack2
            // 
            this.btnTrack2.Location = new System.Drawing.Point(653, 108);
            this.btnTrack2.Name = "btnTrack2";
            this.btnTrack2.Size = new System.Drawing.Size(75, 23);
            this.btnTrack2.TabIndex = 8;
            this.btnTrack2.Text = "Peron2";
            this.btnTrack2.UseVisualStyleBackColor = true;
            this.btnTrack2.Click += new System.EventHandler(this.btnTrack2_Click);
            // 
            // btnTrack3
            // 
            this.btnTrack3.Location = new System.Drawing.Point(909, 108);
            this.btnTrack3.Name = "btnTrack3";
            this.btnTrack3.Size = new System.Drawing.Size(75, 23);
            this.btnTrack3.TabIndex = 9;
            this.btnTrack3.Text = "Peron3";
            this.btnTrack3.UseVisualStyleBackColor = true;
            this.btnTrack3.Click += new System.EventHandler(this.btnTrack3_Click);
            // 
            // tmrTrainLoad
            // 
            this.tmrTrainLoad.Interval = 1000;
            // 
            // lbTrack1
            // 
            this.lbTrack1.FormattingEnabled = true;
            this.lbTrack1.Location = new System.Drawing.Point(333, 46);
            this.lbTrack1.Name = "lbTrack1";
            this.lbTrack1.Size = new System.Drawing.Size(209, 56);
            this.lbTrack1.TabIndex = 10;
            // 
            // lbExit
            // 
            this.lbExit.FormattingEnabled = true;
            this.lbExit.Location = new System.Drawing.Point(585, 386);
            this.lbExit.Name = "lbExit";
            this.lbExit.Size = new System.Drawing.Size(209, 56);
            this.lbExit.TabIndex = 12;
            // 
            // lbStation1
            // 
            this.lbStation1.FormattingEnabled = true;
            this.lbStation1.Location = new System.Drawing.Point(333, 163);
            this.lbStation1.Name = "lbStation1";
            this.lbStation1.Size = new System.Drawing.Size(209, 134);
            this.lbStation1.TabIndex = 14;
            // 
            // lbStation2
            // 
            this.lbStation2.FormattingEnabled = true;
            this.lbStation2.Location = new System.Drawing.Point(585, 163);
            this.lbStation2.Name = "lbStation2";
            this.lbStation2.Size = new System.Drawing.Size(209, 134);
            this.lbStation2.TabIndex = 15;
            // 
            // lbStation3
            // 
            this.lbStation3.FormattingEnabled = true;
            this.lbStation3.Location = new System.Drawing.Point(842, 163);
            this.lbStation3.Name = "lbStation3";
            this.lbStation3.Size = new System.Drawing.Size(209, 134);
            this.lbStation3.TabIndex = 16;
            // 
            // btnStation1
            // 
            this.btnStation1.Location = new System.Drawing.Point(400, 323);
            this.btnStation1.Name = "btnStation1";
            this.btnStation1.Size = new System.Drawing.Size(75, 23);
            this.btnStation1.TabIndex = 17;
            this.btnStation1.Text = "Odlazak";
            this.btnStation1.UseVisualStyleBackColor = true;
            this.btnStation1.Click += new System.EventHandler(this.btnStation1_Click);
            // 
            // btnStation2
            // 
            this.btnStation2.Location = new System.Drawing.Point(653, 323);
            this.btnStation2.Name = "btnStation2";
            this.btnStation2.Size = new System.Drawing.Size(75, 23);
            this.btnStation2.TabIndex = 18;
            this.btnStation2.Text = "Odlazak";
            this.btnStation2.UseVisualStyleBackColor = true;
            this.btnStation2.Click += new System.EventHandler(this.btnStation2_Click);
            // 
            // btnStation3
            // 
            this.btnStation3.Location = new System.Drawing.Point(909, 323);
            this.btnStation3.Name = "btnStation3";
            this.btnStation3.Size = new System.Drawing.Size(75, 23);
            this.btnStation3.TabIndex = 19;
            this.btnStation3.Text = "Odlazak";
            this.btnStation3.UseVisualStyleBackColor = true;
            this.btnStation3.Click += new System.EventHandler(this.btnStation3_Click);
            // 
            // lblExit
            // 
            this.lblExit.AutoSize = true;
            this.lblExit.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblExit.Location = new System.Drawing.Point(531, 467);
            this.lblExit.Name = "lblExit";
            this.lblExit.Size = new System.Drawing.Size(63, 24);
            this.lblExit.TabIndex = 20;
            this.lblExit.Text = "label1";
            // 
            // lblCounter
            // 
            this.lblCounter.AutoSize = true;
            this.lblCounter.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCounter.Location = new System.Drawing.Point(276, 212);
            this.lblCounter.Name = "lblCounter";
            this.lblCounter.Size = new System.Drawing.Size(21, 24);
            this.lblCounter.TabIndex = 21;
            this.lblCounter.Text = "0";
            // 
            // btnSaveToFile
            // 
            this.btnSaveToFile.Location = new System.Drawing.Point(76, 512);
            this.btnSaveToFile.Name = "btnSaveToFile";
            this.btnSaveToFile.Size = new System.Drawing.Size(101, 39);
            this.btnSaveToFile.TabIndex = 22;
            this.btnSaveToFile.Text = "Evidencija";
            this.btnSaveToFile.UseVisualStyleBackColor = true;
            this.btnSaveToFile.Click += new System.EventHandler(this.btnSaveToFile_Click);
            // 
            // btnExitFromApp
            // 
            this.btnExitFromApp.Location = new System.Drawing.Point(968, 512);
            this.btnExitFromApp.Name = "btnExitFromApp";
            this.btnExitFromApp.Size = new System.Drawing.Size(83, 31);
            this.btnExitFromApp.TabIndex = 23;
            this.btnExitFromApp.Text = "Izlaz";
            this.btnExitFromApp.UseVisualStyleBackColor = true;
            this.btnExitFromApp.Click += new System.EventHandler(this.btnExitFromApp_Click);
            // 
            // FormTrack
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1097, 615);
            this.Controls.Add(this.btnExitFromApp);
            this.Controls.Add(this.btnSaveToFile);
            this.Controls.Add(this.lblCounter);
            this.Controls.Add(this.lblExit);
            this.Controls.Add(this.btnStation3);
            this.Controls.Add(this.btnStation2);
            this.Controls.Add(this.btnStation1);
            this.Controls.Add(this.lbStation3);
            this.Controls.Add(this.lbStation2);
            this.Controls.Add(this.lbStation1);
            this.Controls.Add(this.lbExit);
            this.Controls.Add(this.lbTrack1);
            this.Controls.Add(this.btnTrack3);
            this.Controls.Add(this.btnTrack2);
            this.Controls.Add(this.btnTrack1);
            this.Controls.Add(this.lblTimer3);
            this.Controls.Add(this.lblTimer2);
            this.Controls.Add(this.lblTimer1);
            this.Controls.Add(this.lbTrack3);
            this.Controls.Add(this.lbTrack2);
            this.Controls.Add(this.lbTrains);
            this.Name = "FormTrack";
            this.Text = "FormTrack";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbTrains;
        private System.Windows.Forms.ListBox lbTrack2;
        private System.Windows.Forms.ListBox lbTrack3;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.Label lblTimer1;
        private System.Windows.Forms.Label lblTimer2;
        private System.Windows.Forms.Label lblTimer3;
        private System.Windows.Forms.Button btnTrack1;
        private System.Windows.Forms.Button btnTrack2;
        private System.Windows.Forms.Button btnTrack3;
        private System.Windows.Forms.Timer tmrTrainLoad;
        private System.Windows.Forms.ListBox lbTrack1;
        private System.Windows.Forms.ListBox lbExit;
        private System.Windows.Forms.ListBox lbStation1;
        private System.Windows.Forms.ListBox lbStation2;
        private System.Windows.Forms.ListBox lbStation3;
        private System.Windows.Forms.Button btnStation1;
        private System.Windows.Forms.Button btnStation2;
        private System.Windows.Forms.Button btnStation3;
        private System.Windows.Forms.Label lblExit;
        private System.Windows.Forms.Label lblCounter;
        private System.Windows.Forms.Button btnSaveToFile;
        private System.Windows.Forms.Button btnExitFromApp;
    }
}