﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HrvatskeZeljezniceApp{
    public class TrainDB{

        public TrainDB() {
            db = new HrvatskeZeljezniceDB();
        }

        public IEnumerable<Vlak> All() {
            return
                from train in db.Vlakovi
                orderby train.Model
                select train;
        }

        public IEnumerable<Vlak> Filter(string filter) {
             return
                from train in db.Vlakovi
                where train.Model.Contains(filter) || train.TRN.Contains(filter)
                orderby train.Model
                select train;          
        }
        public void Delete(Vlak selectedTrain){
            var toDelete = from train in db.Vlakovi
                           where train.Model.Contains(selectedTrain.Model) && train.TRN.Contains(selectedTrain.TRN)
                           orderby train.Model
                           select train;
            db.Vlakovi.RemoveRange(toDelete);
            db.SaveChanges();
        }


        public void Add(Vlak Train){
            db.Vlakovi.Add(Train);
            db.SaveChanges();
        }

        public Vlak Random(){
            return db.Vlakovi
                     .OrderBy(r => Guid.NewGuid())
                     .FirstOrDefault(); 
        }

        private HrvatskeZeljezniceDB db;
    }
}
