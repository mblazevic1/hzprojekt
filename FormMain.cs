﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HrvatskeZeljezniceApp
{
    public partial class FormMain : Form {
        
        TrainDB trains = new TrainDB();
        TrainManager trainManager = new TrainManager();


        public FormMain() {
            InitializeComponent();
            LoadDB();
        }

        IEnumerable<Vlak> trainList;

        public void LoadDB() {
            lbTrains.Items.Clear();

            if (string.IsNullOrEmpty(tbFilter.Text)) {
                trainList = trains.All();
            }
            else {
                trainList = trains.Filter(tbFilter.Text);
            }
            foreach (var train in trainList) {
                lbTrains.Items.Add(train);
            }
        }

        private void tbFilter_TextChanged(object sender, EventArgs e) {
            lbTrains.Items.Clear();
            LoadDB();
        }

        

        private void btnDelete_Click(object sender, EventArgs e){
            if (lbTrains.SelectedItem is Vlak selectedTrain) {
                trainManager.DeleteTrain(selectedTrain);
                LoadDB();
            } 
        }

        private void btnSave_Click(object sender, EventArgs e){
            string model = tbModel.Text;
            string trn = tbTRN.Text;
            trainManager.AddTrain(new Vlak(model,trn));
            LoadDB();
            tbModel.Text = "";
            tbTRN.Text = "";

        }
        

        private void btnStart_Click(object sender, EventArgs e) {
            FormTrack Form = new FormTrack();
            Form.Show();
            

        }
    }
}
