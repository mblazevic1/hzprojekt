namespace HrvatskeZeljezniceApp
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Vlakovi")]
    public partial class Vlak{

        public Vlak() { }
        public Vlak(string model, string trn) {
            Model = model;
            TRN = trn;
        }

        public override string ToString() {
            return $"{Model}  -  {TRN}";
        }
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Model { get; set; }

        [StringLength(50)]
        public string TRN { get; set; }
    }
}
