﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Text.Json;
using System.IO;
using System.Diagnostics;
using System.Drawing;

namespace HrvatskeZeljezniceApp {
    public partial class FormTrack : Form {

        TrainDB trains = new TrainDB();
        TrackManager trackManager;


        TimerSettings timerSettings = new TimerSettings();
        private int counter = 0;


        public FormTrack() {
            trackManager = new TrackManager(this);
            InitializeComponent();
            LoadRandomTrain();
            DBLoadTimer();
            trackManager.TrainAtStation += TrainAtStation;
            lblExit.Hide();
        }

       

        public void DisableLine(ListBox track, ListBox station, Button moveToStation, Button moveToExit, int line) {
            track.Enabled = false;
            station.Enabled = false;
            moveToStation.Enabled = false;
            moveToExit.Enabled = false;
            track.BackColor = Color.Crimson;
            station.BackColor = Color.Crimson;
            switch (line) {
                case 1:
                    timer1.Stop();
                    break;
                case 2:
                    timer2.Stop();
                    break;
                case 3:
                    timer3.Stop();
                    break;
            }
            track.Items.Clear();
            station.Items.Clear();
        }

        private void SelectedItemCheck(ListBox listBox, Timer timer, ListBox listBoxTrack, Button button) {
            if (listBox.SelectedItem != null) {
                if (!timer.Enabled) {
                    timer.Enabled = true;
                }
            }
        }




        private void btnTrack1_Click(object sender, EventArgs e) {
            SelectedItemCheck(lbTrains, timer1, lbTrack1, btnTrack1);
            trackManager.MoveTrainToStation(lbTrains, lbTrack1, lbStation1, 1);
            
        }

        private void btnTrack2_Click(object sender, EventArgs e) {
            SelectedItemCheck(lbTrains, timer2, lbTrack2, btnTrack2);
            trackManager.MoveTrainToStation(lbTrains, lbTrack2, lbStation2, 2);
        }

        private void btnTrack3_Click(object sender, EventArgs e) {
            SelectedItemCheck(lbTrains, timer3, lbTrack3, btnTrack3);
            trackManager.MoveTrainToStation(lbTrains, lbTrack3, lbStation3, 3);
        }


        
        private void DBLoadTimer() {
            tmrTrainLoad = new Timer();
            tmrTrainLoad.Interval = 1000;
            tmrTrainLoad.Tick += tmrTrainLoad_Tick;
            tmrTrainLoad.Start();
        }


        private void tmrTrainLoad_Tick(object sender, EventArgs e) {
            LoadRandomTrain();
        }

        private void UpdateStatus(int time, Label label) {
            int minutes = time / 60;
            int seconds = time % 60;
            label.Text = string.Format("{0:D2}:{1:D2}", minutes, seconds);

        }
        Vlak train;
        private List<Vlak> loadedTrains = new List<Vlak>();
        private void LoadRandomTrain() {
            Vlak train = trains.Random(); 

            if (!loadedTrains.Contains(train)) {
                lbTrains.Items.Add(train);
                loadedTrains.Add(train);
            }

        }

        private void TrainAtStation(object sender, TrainMovementArgs e) {
            counter++;
            lblCounter.Text = counter.ToString();
        }

        private void btnStation1_Click(object sender, EventArgs e) {
            trackManager.MoveTrainToExit(lbStation1, lbExit, lblExit);
            lblExit.Show();
        }

        private void btnStation2_Click(object sender, EventArgs e) {
            trackManager.MoveTrainToExit(lbStation2, lbExit, lblExit);
            lblExit.Show();
        }

        private void btnStation3_Click(object sender, EventArgs e) {
            trackManager.MoveTrainToExit(lbStation3, lbExit, lblExit);
            lblExit.Show();
        }


        public void CallDisableLine(int line) {
            switch (line) {
                case 1:
                    DisableLine(lbTrack1, lbStation1, btnTrack1, btnStation1, 1);
                    break;
                case 2:
                    DisableLine(lbTrack2, lbStation2, btnTrack2, btnStation2, 2);
                    break;
                case 3:
                    DisableLine(lbTrack3, lbStation3, btnTrack3, btnStation3, 3);
                    break;
            }


        }

        private void btnSaveToFile_Click(object sender, EventArgs e) {
            List<string> stationPasses = trackManager.StationPasses;
            string fileName = "StationPasses.json";
            string jsonString = JsonSerializer.Serialize(stationPasses);
            File.WriteAllText(fileName, jsonString);
            Process.Start(fileName);
        }

        private void btnExitFromApp_Click(object sender, EventArgs e) {
            this.Close();
        }


        private void timer1_Tick_1(object sender, EventArgs e) {
            int time1 = timerSettings.Time1;
            bool timer1 = timerSettings.Timer0;
            TimerTickHandler(ref time1, ref timer1, lblTimer1);
            timerSettings.Time1 = time1;


        }

        private void timer2_Tick_1(object sender, EventArgs e) {
            int time2 = timerSettings.Time2;
            bool timer2 = timerSettings.Timer0;
            TimerTickHandler(ref time2, ref timer2, lblTimer2);
            timerSettings.Time2 = time2;

        }

        private void timer3_Tick_1(object sender, EventArgs e) {
            int time3 = timerSettings.Time3;
            bool timer3 = timerSettings.Timer0;
            TimerTickHandler(ref time3, ref timer3, lblTimer3);
            timerSettings.Time3 = time3;
        }

        private void TimerTickHandler(ref int time, ref bool timer0,Label label) {
            time--;
            UpdateStatus(time, label);
            if (time <= 0) {
                timer0 = true; 
                time += timerSettings.AddTime;
                if (label == lblTimer1) {
                    trackManager.MoveTrainToStation(lbTrains, lbTrack1, lbStation1, 1);
                } else if(label == lblTimer2) {
                    trackManager.MoveTrainToStation(lbTrains, lbTrack2, lbStation2, 2);
                }else {
                    trackManager.MoveTrainToStation(lbTrains, lbTrack3, lbStation3, 1);
                }

                if (timerSettings.AddTime > 10) {
                    timerSettings.AddTime -= 5; 
                }
                
            }
        }



    }
}




